import React, { useState } from 'react'
import { InlineForm, Button, ListItem } from './Components'
import './App.css'

function AddPerson({ addPerson }) {
  const [name, setName] = useState('')
  const handleSubmit = e => {
    e.preventDefault()
    addPerson(name)
    setName('')
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Person name:{' '}
        <input
          value={name}
          name='person'
          onChange={e => setName(e.target.value)}
        />
        <Button primary type='submit'>
          Add
        </Button>
      </label>
    </form>
  )
}

function Person({ person, onRemove, onEdit, onSave }) {
  const [name, setName] = useState(person.name)
  const handleSave = e => {
    e.preventDefault()
    onSave(name)
    setName('')
  }

  if (person.editing) {
    return (
      <InlineForm onSubmit={handleSave}>
        <input value={name} onChange={e => setName(e.target.value)} />
        <Button primary type='submit'>
          Save
        </Button>
      </InlineForm>
    )
  } else {
    return (
      <span>
        {person.name}
        <Button onClick={onRemove}>Delete</Button>
        <Button onClick={onEdit}>Edit</Button>
      </span>
    )
  }
}

function ListPersons({ persons, onRemovePerson, onEditPerson, onSavePerson }) {
  return (
    <ul>
      {persons.map((person, id) => (
        <ListItem key={id}>
          <Person
            person={person}
            onRemove={onRemovePerson.bind(null, id)}
            onEdit={onEditPerson.bind(null, id)}
            onSave={onSavePerson.bind(null, id)}
          />
        </ListItem>
      ))}
    </ul>
  )
}

function App() {
  const [persons, setPersons] = useState([])
  const addPerson = name =>
    setPersons([
      ...persons,
      {
        editing: false,
        name,
      },
    ])

  const removePerson = id => {
    console.log(`deleting person: id=${id}, name=${persons[id].name}`)
    setPersons([...persons.slice(0, id), ...persons.slice(id + 1)])
  }

  const editPerson = id => {
    setPersons([
      ...persons.slice(0, id),
      {
        ...persons[id],
        editing: true,
      },
      ...persons.slice(id + 1),
    ])
  }

  const savePerson = (id, name) => {
    setPersons([
      ...persons.slice(0, id),
      {
        editing: false,
        name,
      },
      ...persons.slice(id + 1),
    ])
  }

  return (
    <div className='App'>
      <header className='App-header'>
        <h1>Group Maker</h1>
        <p>Create groups of persons</p>
      </header>
      <div>
        <AddPerson addPerson={addPerson} />
        <ListPersons
          persons={persons}
          onRemovePerson={removePerson}
          onEditPerson={editPerson}
          onSavePerson={savePerson}
        />
      </div>
    </div>
  )
}

export default App
