import styled from 'styled-components'

const foreground = ({ primary }) => (primary ? 'white' : 'black')
const background = ({ primary }) => (primary ? '#24a' : 'white')
const border = ({ primary }) => (primary ? 'none' : '1px solid black')

export const InlineForm = styled.form`
  display: inline-block;
`

export const ListItem = styled.li`
  list-style: none;
`

export const Button = styled.button`
  border: ${border};
  padding: 6px 12px;
  border-radius: 4px;
  background-color: ${background};
  color: ${foreground};
`
